//
//  WebViewController.swift
//  avaliacaoAvancado
//
//  Created by Dainf on 17/02/16.
//  Copyright (c) 2016 br.edu.utfpr. All rights reserved.
//

import UIKit

class WebViewController: UIViewController {
    
    @IBOutlet weak var web_view: UIWebView!

    override func viewDidLoad() {
        let preferences = NSUserDefaults.standardUserDefaults()
        let currentLevelKey = "firstAccess"

        print("Verificando login\n")
        let loginLevelKey = "loginKey"
        let passLevelKey = "senhaKey"
        let login: String = preferences.stringForKey(loginLevelKey)!
        let pass: String = preferences.stringForKey(passLevelKey)!
        
        super.viewDidLoad()
        let url = NSURL (string: "http://www.autogerenciador.com.br/login.aspx?acao=MOBILE&Login=\(login)&Senha=\(pass)")
        let requestObj = NSURLRequest(URL: url!)
        web_view.loadRequest(requestObj)

        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func logout(){
        
        let preferences = NSUserDefaults.standardUserDefaults()
        preferences.removeObjectForKey("loginKey")
        preferences.removeObjectForKey("senhaKey")
        preferences.synchronize()
        
        self.navigationController?.popToRootViewControllerAnimated(true)        
    }
    



}
