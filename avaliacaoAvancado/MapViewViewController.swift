//
//  MapViewViewController.swift
//  avaliacaoAvancado
//
//  Created by Dainf on 09/03/16.
//  Copyright © 2016 br.edu.utfpr. All rights reserved.
//

import UIKit
import MapKit

class MapViewViewController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate {

     var locationManager: CLLocationManager!
    
    @IBOutlet var mapView: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (CLLocationManager.locationServicesEnabled())
        {
            locationManager = CLLocationManager()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestAlwaysAuthorization()
            locationManager.startUpdatingLocation()
        }
        
        mapView.delegate = self
        mapView.showsUserLocation = true
        mapView.userTrackingMode = MKUserTrackingMode(rawValue: 2)!
        
    }
    
    
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        _ = locations.last! as CLLocation
        
        CLGeocoder().reverseGeocodeLocation(manager.location!, completionHandler: {(placemarks, error) -> Void in
            if (error != nil) {
                print("Reverse geocoder failed with error" + error!.localizedDescription, terminator: "")
                return
            }
            
            if placemarks!.count > 0 {
                _ = placemarks![0] as CLPlacemark
                
            }else {
                print("Problem with data received", terminator: "")
            }
            
            let center = CLLocationCoordinate2D(latitude: -25.447218, longitude:  -49.265330)
            let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
            
            self.mapView.setRegion(region, animated: true)
            
            
        })
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
