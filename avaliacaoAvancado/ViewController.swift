//
//  ViewController.swift
//  avaliacao_avancado
//
//  Created by Dainf on 17/02/16.
//  Copyright (c) 2016 br.com.dainf. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


class ViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet var txt_login: UITextField!
    @IBOutlet var txt_senha: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.txt_login.delegate = self
        self.txt_senha.delegate = self
        
    }
    
    @IBAction func doLogin() -> Void
    {
        print("Fazendo login...")
        print("Login: \(self.txt_login.text)\nSenha: \(self.txt_senha.text)")
        
        //salvar preferencias
        
        let preferences = NSUserDefaults.standardUserDefaults()
        let loginKey = self.txt_login.text
        let senhaKey = self.txt_senha.text
        
        
        
        //login>>> pedro@caminhoesecarretas.com.br
        //senha>>> 1111
        
        if (loginKey!.isEmpty || senhaKey!.isEmpty)
        {
        showMessage("ALERTA", message: "Favor colocar login ou senha.")
        }
        else {
            Alamofire.request(.GET, "http://autogerenciador.com.br/AutenticaApp.aspx", parameters: ["Login": loginKey!, "Senha": senhaKey!])
            .responseJSON { response in
                print(response.request)
                print(response.response)
                print(response.data)
                print(response.result)
                if let JSON = response.result.value {
                    print("*****JSON: \(JSON)")
                }
            }
        }
        
        
        
        preferences.setValue(loginKey, forKey: "loginKey")
        preferences.setValue(senhaKey, forKey: "senhaKey")
        preferences.synchronize()
        
    }
    
    func showMessage(title: String, message: String) -> Void
    {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        let defaultAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
        alertController.addAction(defaultAction)
        presentViewController(alertController, animated: true, completion: nil)
    }
    
    
    @IBAction func forgotPassword() -> Void
    {
        print("teje clicado em ESQUECI MINHA SENHA")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if(textField == txt_login){
            txt_senha.becomeFirstResponder()
        } else if (textField == txt_senha){
            txt_senha.resignFirstResponder()
        }
        
        return true
    }
    
    
}

